from django.contrib import admin
from django.urls import path

from newsletter_api.api import (
    ClientCreateApi, ClientUpdateApi, ClientDeleteApi,
    NewsletterCreateApi, NewsletterUpdateApi, NewsletterDeleteApi,
    StatisticsApi, NewsletterStatisticsApi,
)

urlpatterns = [
    path('admin/', admin.site.urls),

    path('api/client/create', ClientCreateApi.as_view()),
    path('api/client/<int:pk>', ClientUpdateApi.as_view()),
    path('api/client/delete/<int:pk>', ClientDeleteApi.as_view()),

    path('api/newsletter/create', NewsletterCreateApi.as_view()),
    path('api/newsletter/<int:pk>', NewsletterUpdateApi.as_view()),
    path('api/newsletter/delete/<int:pk>', NewsletterDeleteApi.as_view()),

    path('api/newsletter/statistics/<int:pk>', NewsletterStatisticsApi.as_view()),
    path('api/statistics', StatisticsApi.as_view()),
]
