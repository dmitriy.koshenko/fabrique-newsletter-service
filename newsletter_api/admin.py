from django.contrib import admin
from newsletter_api.models import (
    Newsletter,
    Client,
    Message,
)

class NewsletterAdmin(admin.ModelAdmin):
    pass

class ClientAdmin(admin.ModelAdmin):
    pass

class MessageAdmin(admin.ModelAdmin):
    pass


admin.site.register(Newsletter, NewsletterAdmin)
admin.site.register(Client, ClientAdmin)
admin.site.register(Message, MessageAdmin)
