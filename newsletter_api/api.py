from django.db.models import Count, Q

from rest_framework import generics
from rest_framework.response import Response

from .serializers import NewsletterSerializer, ClientSerializer
from .models import Newsletter, Client, Message

from .sender import Sender


class ClientCreateApi(generics.CreateAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer

class ClientUpdateApi(generics.UpdateAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer

class ClientDeleteApi(generics.DestroyAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class NewsletterCreateApi(generics.CreateAPIView):
    queryset = Newsletter.objects.all()
    serializer_class = NewsletterSerializer

    def perform_create(self, serializer):
        newsletter = serializer.save()

        filters = (newsletter.filter).strip().split(',')
        for client in Client.objects.filter(Q(operator_code__in=filters) | Q(tag__in=filters)):
            new_message = Message(newsletter=newsletter, client=client)
            new_message.save()

            Sender.send(new_message)

class NewsletterUpdateApi(generics.UpdateAPIView):
    queryset = Newsletter.objects.all()
    serializer_class = NewsletterSerializer

class NewsletterDeleteApi(generics.DestroyAPIView):
    queryset = Newsletter.objects.all()
    serializer_class = NewsletterSerializer


class StatisticsApi(generics.GenericAPIView):
    def get(self, request, *args, **kwargs):
        statistics = Message.objects.values('status').annotate(total=Count('id'))
        return Response(statistics)

class NewsletterStatisticsApi(generics.GenericAPIView):
    def get(self, request, *args, **kwargs):
        statistics = Message.objects.filter(newsletter_id=kwargs.get('pk')).values('status').annotate(total=Count('id'))
        return Response(statistics)
