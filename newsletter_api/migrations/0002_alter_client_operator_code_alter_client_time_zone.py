# Generated by Django 4.0.2 on 2022-03-17 13:24

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('newsletter_api', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='client',
            name='operator_code',
            field=models.CharField(max_length=3, validators=[django.core.validators.MinLengthValidator(3, message='Operator code must have 3 characters.')]),
        ),
        migrations.AlterField(
            model_name='client',
            name='time_zone',
            field=models.IntegerField(validators=[django.core.validators.MinValueValidator(-12, message='Time zone must be more then -11.'), django.core.validators.MaxValueValidator(14, message='Time zone must less then +15.')]),
        ),
    ]
