
from django.db import models

from django.core.validators import (
    RegexValidator,
    MaxValueValidator,
    MinValueValidator,
    MinLengthValidator,
)

class MessageStatus:
    NEW = 'NEW'
    IN_QUEUE = 'IN_QUEUE'
    DELIVERED = 'DELIVERED'
    LOST = 'LOST'

class Newsletter(models.Model):
    start_time = models.DateTimeField(blank=False)
    end_time = models.DateTimeField(blank=False)
    text = models.CharField(max_length=160, blank=False)
    filter = models.CharField(max_length=160, blank=False)

class Client(models.Model):
    phone = models.CharField(
        validators=[RegexValidator(regex=r'7\d{10}', message="Phone number must be entered in the format: '7XXXXXXXXXX'.")],
        max_length=11, blank=False)
    operator_code = models.CharField(validators=[
            MinLengthValidator(3),
        ],
        max_length=3, blank=False)
    tag = models.CharField(max_length=160, blank=False)
    time_zone = models.IntegerField(
        validators=[
            MinValueValidator(-12, message="Time zone must be more then -11."),
            MaxValueValidator(14, message="Time zone must less then +15."),
        ], blank=False)

class Message(models.Model):
    create_time = models.DateTimeField(blank=False, auto_now_add=True)
    status = models.CharField(max_length=160, blank=False, default=MessageStatus.NEW)
    newsletter = models.ForeignKey(Newsletter, on_delete=models.DO_NOTHING)
    client = models.ForeignKey(Client, on_delete=models.DO_NOTHING)
