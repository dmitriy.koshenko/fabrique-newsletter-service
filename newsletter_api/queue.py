import pika
import logging

from django.core import serializers

from .models import Message

class Queue():
    queue_name = 'sender'

    @classmethod
    def send(cls, message: Message):
        text_message = serializers.serialize('json', [ message, ])

        connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
        channel = connection.channel()

        channel.queue_declare(queue=cls.queue_name)
        channel.basic_publish(exchange='', routing_key=cls.queue_name, body=text_message)

        connection.close()

        logging.info("Queue message was sent")
