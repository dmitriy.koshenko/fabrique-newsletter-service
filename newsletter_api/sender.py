import requests
import logging

import pytz
from datetime import datetime, timedelta

from .models import Message, MessageStatus, Client, Newsletter

from .queue import Queue
from .external_api import ExternalApi

logging.basicConfig(filename='sender.log', encoding='utf-8', level=logging.INFO)


class Sender():
    @classmethod
    def send(self, message: Message):
        client = Client.objects.get(id=message.client.id)
        newsletter = Newsletter.objects.get(id=message.newsletter.id)

        current_time = pytz.utc.localize(datetime.now() + timedelta(hours=client.time_zone))
        if newsletter.start_time > current_time:
            message.status = MessageStatus.LOST
            logging.info(f"Message {message.id} status {message.status}")


        response = requests.post(
            f'{ExternalApi.api_url}/{message.id}',
            json={'id': message.id, 'phone': int(client.phone), 'text': newsletter.text},
            headers={'Authorization': f'access_token {ExternalApi.api_token}'}
        )

        if response.status_code == 200:
            message = MessageStatus.DELIVERED
            logging.info(f"Message {message.id} status {message.status}")
        else:
            Queue.send(message.id)
            message.status = MessageStatus.IN_QUEUE
            logging.info(f"Message {message.id} status {message.status}")
