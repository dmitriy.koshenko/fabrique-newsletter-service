from rest_framework import serializers
from .models import Newsletter, Client

class NewsletterSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()
    class Meta:
        model = Newsletter
        fields = '__all__'

class ClientSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()
    class Meta:
        model = Client
        fields = '__all__'
