
import pika
import json

import logging
import functools
import threading
import requests

from datetime import datetime

logging.basicConfig(filename='sender.log', encoding='utf-8', level=logging.INFO)

class Sender():
    api_url = 'https://probe.fbrq.cloud/docs'
    api_token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2Nzg5NTc5MDMsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6IkRtaXRyaXlLb3NoZW5rbyJ9.ZmhJlym7Lvxgdszgqf0t_qq7bxRVmF0vZzuPmag1BGs'
    queue_name = 'sender'

    def send_message(self, connection, channel, delivery_tag, body) -> str:
        thread_id = threading.get_ident()
        logging.info('Thread id: ', thread_id, 'Delivery tag: ', delivery_tag, 'Message body: ', body)

        message_json = json.loads(body)
        try:
            message = SenderMessage(
                message_json.get('msg_id'),
                message_json.get('phone'),
                message_json.get('text'),
                message_json.get('msg_id'),
            )
        except:
            return

        logging.info('Sending msg_id: ', message.msg_id,
            'phone: ', message.phone, 'text: ', message.text, 'end_time: ', message.end_time)

        response = requests.post(
            f'{self.api_url}/{message.msg_id}',
            json={'id': message.msg_id, 'phone': message.phone, 'text': message.text},
            headers={'Authorization': f'access_token {self.api_token}'}
        )
        if response != 200:
            channel.basic_ack(delivery_tag)
        else:
            channel.basic_nack(delivery_tag)

    def on_message(self, channel, method_frame, header_frame, body, args):
        connection, threads = args
        delivery_tag = method_frame.delivery_tag

        t = threading.Thread(target=self.send_message, args=(connection, channel, delivery_tag, body))
        t.start()
        threads.append(t)


    def run(self):
        connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))

        channel = connection.channel()
        channel.queue_declare(queue=self.queue_name, durable=True, auto_delete=False)

        channel.basic_qos(prefetch_count=1)

        threads = []
        on_message_callback = functools.partial(self.on_message, args=(connection, threads))

        channel.basic_consume(self.queue_name, on_message_callback)
        channel.start_consuming()

        for thread in threads:
            thread.join()

if __name__ == '__main__':
    sender = Sender()
    sender.run()
